const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
  if (req.body.hasOwnProperty('id')) {
    req.valid = false;
  } else {
    req.valid = result = createCheck(req.body);
  }
  next();
};

const updateFighterValid = (req, res, next) => {
  if (req.body.hasOwnProperty('id')) {
    req.valid = false;
  } else {
    req.valid = result = updateCheck(req.body);
  }
  next();
};

const createCheck = body => {
  const fields = Object.keys(body);
  const fighterEntries = Object.entries(fighter);

  if (fields.length === 0) return false;

  const noExcess = fields.every(field => fighter.hasOwnProperty(field));

  if (!noExcess) return false;

  const required = fighterEntries.every(entry =>
    entry[1].required ? fields.includes(entry[0]) : true
  );
  const rules = fields.every(field => {
    if (fighter[field].checks === undefined) return true;

    const checks = Object.entries(fighter[field].checks);

    return checks.every(([name, condition]) => {
      switch (name) {
        case 'regexp':
          return condition.test(body[field]);
        case 'min':
          return body[field].length >= condition;
        case 'max':
          return body[field].length < condition;
        case 'number':
          const value = parseInt(body[field]);
          if (Number.isNaN(value)) return false;
          return value < condition.max && value > condition.min;
      }
    });
  });

  return noExcess && required && rules;
};

const updateCheck = body => {
  const fields = Object.keys(body);

  if (fields.length === 0) return false;
  const noExcess = fields.every(field => fighter.hasOwnProperty(field));

  if (!noExcess) return false;

  const rules = fields.every(field => {
    if (fighter[field].checks === undefined) return true;

    const checks = Object.entries(fighter[field].checks);

    return checks.every(([name, condition]) => {
      switch (name) {
        case 'regexp':
          return condition.test(body[field]);
        case 'min':
          return body[field].length >= condition;
        case 'max':
          return body[field].length < condition;
        case 'number':
          const value = parseInt(body[field]);
          if (Number.isNaN(value)) return false;
          return value < condition.max && value > condition.min;
      }
    });
  });

  return rules && noExcess;
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
