const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
  if (req.body.hasOwnProperty('id')) {
    req.valid = false;
  } else {
    req.valid = result = createCheck(req.body);
  }
  next();
};

const updateUserValid = (req, res, next) => {
  if (req.body.hasOwnProperty('id')) {
    req.valid = false;
  } else {
    req.valid = result = updateCheck(req.body);
  }
  next();
};

const createCheck = body => {
  const fields = Object.keys(body);
  const userEntries = Object.entries(user);

  if (fields.length === 0) return false;

  const noExcess = fields.every(field => user.hasOwnProperty(field));
  if (!noExcess) return false;
  const required = userEntries.every(entry =>
    entry[1].required ? fields.includes(entry[0]) : true
  );
  const rules = fields.every(field => {
    if (user[field].checks === undefined) return true;

    const checks = Object.entries(user[field].checks);

    return checks.every(([name, condition]) => {
      switch (name) {
        case 'regexp':
          return condition.test(body[field]);
        case 'min':
          return body[field].length >= condition;
        case 'max':
          return body[field].length < condition;
      }
    });
  });

  return noExcess && required && rules;
};

const updateCheck = body => {
  const fields = Object.keys(body);

  if (fields.length === 0) return false;
  const noExcess = fields.every(field => user.hasOwnProperty(field));
  if (!noExcess) return false;

  const rules = fields.every(field => {
    if (user[field].checks === undefined) return true;

    const checks = Object.entries(user[field].checks);

    return checks.every(([name, condition]) => {
      switch (name) {
        case 'regexp':
          return condition.test(body[field]);
        case 'min':
          return body[field].length >= condition;
        case 'max':
          return body[field].length < condition;
      }
    });
  });

  return rules && noExcess;
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
