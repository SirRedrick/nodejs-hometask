const responseMiddleware = (req, res, next) => {
  const { error, status = 200, message, data } = res.locals;
  if (error) {
    res.status(status).json({ error, message });
  } else {
    res.status(status).json(data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
