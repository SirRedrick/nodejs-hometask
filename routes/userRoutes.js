const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  const result = UserService.getAll();
  if (result) {
    res.locals = { error: false, data: result };
  } else {
    res.locals = { error: true, status: 500, message: 'Something went wrong' };
  }
  next();
});

router.get('/:id', (req, res, next) => {
  const result = UserService.search({ id: req.params.id });
  if (result) {
    res.locals = { error: false, data: result };
  } else {
    res.locals = { error: true, status: 404, message: 'User not found' };
  }
  next();
});

router.post('/', createUserValid, (req, res, next) => {
  if (req.valid) {
    const result = UserService.create(req.body);
    if (result.error) {
      res.locals = { error: result.error, status: 400, message: result.message };
    } else {
      res.locals = { error: false, data: result };
    }
  } else {
    res.locals = { error: true, status: 400, message: 'User entity to create is invalid' };
  }
  next();
});

router.put('/:id', updateUserValid, (req, res, next) => {
  if (UserService.search({ id: req.params.id })) {
    if (req.valid) {
      const result = UserService.update(req.params.id, req.body);
      if (result.error) {
        res.locals = { error: result.error, status: 400, message: result.message };
      } else {
        res.locals = { error: false, data: result };
      }
    } else {
      res.locals = { error: true, status: 400, message: 'User entity to create is invalid' };
    }
  } else {
    res.locals = { error: true, status: 404, message: 'User not found' };
  }
  next();
});

router.delete('/:id', (req, res, next) => {
  if (UserService.search({ id: req.params.id })) {
    UserService.delete(req.params.id);
    res.locals = { error: false, status: 204, data: {} };
  } else {
    res.locals = { error: true, status: 404, message: 'User not found' };
  }
  next();
});

router.use(responseMiddleware);

module.exports = router;
