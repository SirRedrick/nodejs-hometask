const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/login',
  (req, res, next) => {
    try {
      const { email, password } = req.body;
      const data = AuthService.login({ email, password });
      res.locals = { error: false, data };
    } catch (err) {
      res.locals = { error: true, status: 404, message: 'No user with such credentials' };
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
