const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  const result = FighterService.getAll();
  res.locals = { error: false, data: result };
  next();
});

router.get('/:id', (req, res, next) => {
  const result = FighterService.search({ id: req.params.id });
  if (result) {
    res.locals = { error: false, data: result };
  } else {
    res.locals = { error: true, status: 404, message: 'Fighter not found' };
  }
  next();
});

router.post('/', createFighterValid, (req, res, next) => {
  if (req.valid) {
    const result = FighterService.create(req.body);
    if (result.error) {
      res.locals = { error: result.error, status: 400, message: result.message };
    } else {
      res.locals = { error: false, data: result };
    }
  } else {
    res.locals = { error: true, status: 400, message: 'Fighter entity to create is invalid' };
  }
  next();
});

router.put('/:id', updateFighterValid, (req, res, next) => {
  if (FighterService.search({ id: req.params.id })) {
    if (req.valid) {
      const result = FighterService.update(req.params.id, req.body);
      if (result.error) {
        res.locals = { error: result.error, status: 400, message: result.message };
      } else {
        res.locals = { error: false, status: 200, data: result };
      }
    } else {
      res.locals = { error: true, status: 400, message: 'Fighter entity to create is invalid' };
    }
  } else {
    res.locals = { error: true, status: 404, message: 'Fighter not found' };
  }
  next();
});

router.delete('/:id', (req, res, next) => {
  if (FighterService.search({ id: req.params.id })) {
    FighterService.delete(req.params.id);
    res.locals = { error: false, status: 200, data: {} };
  } else {
    res.locals = { error: true, status: 404, message: 'Fighter not found' };
  }
  next();
});

router.use(responseMiddleware);

module.exports = router;
