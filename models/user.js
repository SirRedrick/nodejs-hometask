exports.user = {
  id: { required: false },
  firstName: { required: true, checks: { regexp: /^[a-zA-Z]+$/, min: 3, max: 100 } },
  lastName: { required: true, checks: { regexp: /^[a-zA-Z]+$/, min: 3, max: 100 } },
  email: {
    required: true,
    checks: { regexp: /^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/, min: 3, max: 100 },
  },
  phoneNumber: { required: true, checks: { regexp: /^\+380[0-9]{9}/ } },
  password: { required: true, checks: { regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/, min: 3 } }, // min 3 symbols
};
