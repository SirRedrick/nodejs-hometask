exports.fighter = {
  id: { required: false },
  name: { required: true, checks: { regexp: /^[a-zA-Z]+$/, min: 3, max: 100 } },
  health: { required: false, default: 100, checks: { number: { min: 80, max: 120 } } },
  power: { required: true, checks: { number: { min: 1, max: 100 } } },
  defense: { required: true, checks: { number: { min: 1, max: 10 } } }, // 1 to 10
};
