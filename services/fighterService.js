const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAll() {
    return FighterRepository.getAll();
  }
  search(query) {
    const item = FighterRepository.getOne(query);
    if (!item) {
      return null;
    }
    return item;
  }
  create(fighter) {
    const nameValidation = FighterRepository.getOne({ name: fighter.name });
    if (nameValidation) {
      return { error: true, message: 'Fighter with this name already exists' };
    } else {
      fighter.health = fighter.hasOwnProperty('health') ? fighter.health : 100;
      return FighterRepository.create(fighter);
    }
  }
  update(id, fighter) {
    const nameValidation = FighterRepository.getOne({ name: fighter.name });
    if (nameValidation) {
      return { error: true, message: 'Fighter with this name already exists' };
    } else {
      return FighterRepository.update(id, fighter);
    }
  }
  delete(id) {
    FighterRepository.delete(id);
  }
}

module.exports = new FighterService();
