const { UserRepository } = require('../repositories/userRepository');

class UserService {
  search(query) {
    const item = UserRepository.getOne(query);
    if (!item) {
      return null;
    }
    return item;
  }

  getAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  create(user) {
    let error = this.isEmailInvalid(user.email);
    if (error) return error;

    error = this.isPhoneNumberInvalid(user.phoneNumber);
    if (error) return error;

    return UserRepository.create(user);
  }

  update(id, user) {
    let error = this.isEmailInvalid(user.email);
    if (error) return error;

    error = this.isPhoneNumberInvalid(user.phoneNumber);
    if (error) return error;

    return UserRepository.update(id, user);
  }

  delete(id) {
    UserRepository.delete(id);
  }

  isEmailInvalid(email) {
    if (UserRepository.getOne({ email })) {
      return { error: true, message: 'User with this email already exists' };
    } else {
      return false;
    }
  }

  isPhoneNumberInvalid(phoneNumber) {
    if (UserRepository.getOne({ phoneNumber })) {
      return { error: true, message: 'User with this phone number already exists' };
    } else {
      return false;
    }
  }
}

module.exports = new UserService();
